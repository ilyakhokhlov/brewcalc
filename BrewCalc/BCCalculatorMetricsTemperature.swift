//
//  BCCalculatorMetricsTemperature.swift
//  BrewCalc
//
//  Created by Ilya on 08.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

class BCCalculatorMetricsTemperature: BCCalculator {
    override init () {
        super.init()
        self.calculatorName = l("metrics.temperature")
        self.inputData = [
            BCNumberEntity(title: l("calc.metrics.celsius"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.metrics.fahrenheit"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.metrics.kelvin"), value: 0.0, numberOfDigits:1)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        self.outputData = self.inputData
        
        var source = 0.0
        if (fromIndex < self.inputData.count) {
            source = (self.inputData[fromIndex] as! BCNumberEntity).numberValue
        }
        
        var cels = 0.0
        switch(fromIndex) {
        case 0:
            cels = source
        case 1:
            cels = self.celsiiFromFarenheit(source)
        case 2:
            cels = self.celsiiFromKelvin(source)
        default:()
        }
        
        (self.outputData[0] as! BCNumberEntity).numberValue = cels
        (self.outputData[1] as! BCNumberEntity).numberValue = self.farenheitFromCelsii(cels)
        (self.outputData[2] as! BCNumberEntity).numberValue = self.kelvinFromCelsii(cels)
    }
    
    func celsiiFromFarenheit(_ source:Double) -> Double {
        return round(10.0*(source-32.0)*5.0/9.0)/10.0
    }
    
    func celsiiFromKelvin(_ source:Double) -> Double {
        return round(10.0*(source-273.15))/10.0
    }
    
    func farenheitFromCelsii(_ source:Double) -> Double {
        return round(10.0*(source*9.0/5.0+32.0))/10.0
    }
    
    func kelvinFromCelsii(_ source:Double) -> Double {
        return round(10.0*(source+273.15))/10.0
    }
}
