//
//  BCNumberEntityCell.swift
//  BrewCalc
//
//  Created by Ilya on 04.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCNumberEntityCell: BCEntityCell {
    @IBOutlet weak var numberTitle: UILabel?
    @IBOutlet weak var numberValue: UITextField?
    
    override func configureWithEntity(_ entity: BCEntity) {
        if let numberEntity = entity as? BCNumberEntity {
            self.numberTitle!.text = numberEntity.numberTitle
            self.numberValue!.text = String(format: String(format: "%%.%if", arguments: [numberEntity.numberOfDigits]), arguments: [numberEntity.numberValue]);
        }
    }
    
    override func setEnabled(_ enabled: Bool) {
        self.numberValue!.isEnabled = enabled
    }
    
    override func becomeResponder() {
        self.numberValue!.becomeFirstResponder()
    }
}
