//
//  BCWebViewController.swift
//  BrewCalc
//
//  Created by Ilya on 21.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation
import UIKit

class BCWebViewController: UIViewController {
    @IBOutlet weak var webView:UIWebView?
    var filename:String!
    override func viewDidLoad() {
        let filepath = Bundle.main.path(forResource: self.filename, ofType: "html")
        let request = URLRequest(url: URL(fileURLWithPath: filepath!))
        
        self.webView?.loadRequest(request)
        
        self.navigationItem.title = l("instruction.title")
        
        let leftButton = UIBarButtonItem(title: l("button.close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BCWebViewController.close))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    @objc func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
