//
//  BCSegmentedControlCell.swift
//  BrewCalc
//
//  Created by Ilya on 04.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCSegmentedControlCell: BCEntityCell {
    @IBOutlet weak var segmentedControl: UISegmentedControl?
    
    override func configureWithEntity(_ entity: BCEntity) {
        if let segmentedEntity = entity as? BCSegmentedEntity {
            self.segmentedControl!.removeAllSegments()
            for segment in segmentedEntity.segments {
                self.segmentedControl!.insertSegment(withTitle: segment, at: self.segmentedControl!.numberOfSegments, animated: false)
            }
            self.segmentedControl!.selectedSegmentIndex = segmentedEntity.selectedIndex
        }
    }
}
