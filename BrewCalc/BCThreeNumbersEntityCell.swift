//
//  BCThreeNumbersEntityCell.swift
//  BrewCalc
//
//  Created by Ilya on 04.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCThreeNumbersEntityCell: BCEntityCell {
    @IBOutlet weak var numberTitle: UILabel?
    @IBOutlet weak var numberTitle1: UILabel?
    @IBOutlet weak var numberTitle2: UILabel?
    @IBOutlet weak var numberTitle3: UILabel?
    @IBOutlet weak var numberValue1: UITextField?
    @IBOutlet weak var numberValue2: UITextField?
    @IBOutlet weak var numberValue3: UITextField?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.numberValue1!.keyboardType = UIKeyboardType.numbersAndPunctuation
        self.numberValue2!.keyboardType = UIKeyboardType.numbersAndPunctuation
        self.numberValue3!.keyboardType = UIKeyboardType.numbersAndPunctuation
    }

    override func configureWithEntity(_ entity: BCEntity) {
        if let threeNumbersEntity = entity as? BCThreeNumbersEntity {
            self.numberTitle!.text = threeNumbersEntity.numberTitle
            self.numberTitle1!.text = threeNumbersEntity.number1.numberTitle
            self.numberTitle2!.text = threeNumbersEntity.number2.numberTitle
            self.numberTitle3!.text = threeNumbersEntity.number3.numberTitle
            self.numberValue1!.text = String(format: String(format: "%%.%if", arguments: [threeNumbersEntity.number1.numberOfDigits]), arguments: [threeNumbersEntity.number1.numberValue])
            self.numberValue2!.text = String(format: String(format: "%%.%if", arguments: [threeNumbersEntity.number2.numberOfDigits]), arguments: [threeNumbersEntity.number2.numberValue])
            self.numberValue3!.text = String(format: String(format: "%%.%if", arguments: [threeNumbersEntity.number3.numberOfDigits]), arguments: [threeNumbersEntity.number3.numberValue])
        }
    }
    
    override func setEnabled(_ enabled: Bool) {
        self.numberValue1!.isEnabled = enabled
        self.numberValue2!.isEnabled = enabled
        self.numberValue3!.isEnabled = enabled
    }
    
    override func becomeResponder() {
        self.numberValue1!.becomeFirstResponder()
    }
}
