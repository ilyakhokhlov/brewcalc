//
//  BCAboutController.swift
//  BrewCalc
//
//  Created by Ilya on 21.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class BCAboutController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var emailLabel:UILabel?
    @IBOutlet weak var emailButton:UIButton?
    @IBOutlet weak var copyrightLabel:UILabel?
    
    @IBAction func openEmail(_ sender:UIButton) {
        let mailController = MFMailComposeViewController();
        mailController.modalPresentationStyle = UIModalPresentationStyle.formSheet
        mailController.mailComposeDelegate = self
        
        if (MFMailComposeViewController.canSendMail()) {
            mailController.setSubject(l("mail.subject"))
            mailController.setToRecipients([bcEmail])
            self.present(mailController, animated: true, completion: nil)
        } else {
            UIPasteboard.general.string = bcEmail
            let alertView = UIAlertView(title: l("about.alert.email.cant.send.title"), message: l("about.alert.email.cant.send.message"), delegate: nil, cancelButtonTitle: l("OK"))
            alertView.show()
        }
    }
    
    override func viewDidLoad() {
        let leftButton = UIBarButtonItem(title: l("button.close"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BCAboutController.close))
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationItem.title = l("about.title")
        
        self.emailLabel?.text = l("about.label.email")
        self.emailButton?.setTitle(bcEmail, for: UIControlState())
        self.copyrightLabel?.text = l("about.label.copyright")
    }
    
    @objc func close() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

