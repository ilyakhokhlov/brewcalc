//
//  BCCalculatorAlkoholFormula.swift
//  BrewCalc
//
//  Created by Ilya on 12.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCCalculatorAlkoholFormula: BCCalculatorSeparated {
    override init () {
        super.init()
        self.calculatorName = l("calc.alcohol.formula.input")
        self.secondName = l("calc.alcohol.formula.result")
        self.inputData = [
            BCSegmentedEntity(segments: [l("segment.units.plato"), l("segment.units.gravity")], selectedIndex: 0),
            BCSegmentedEntity(segments: [l("segment.units.celsii"), l("segment.units.farenheit")], selectedIndex: 0),
            BCNumberEntity(title: l("calc.alcohol.formula.plato.begin"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.alcohol.formula.temp.begin"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.alcohol.formula.plato.end"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.alcohol.formula.temp.end"), value: 0.0, numberOfDigits:1)]
        self.outputData = [
            BCNumberEntity(title: l("calc.alcohol.formula.abv"), value: 0.0, numberOfDigits:2)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        let gravityCalc = BCCalculatorGravity()
        let tempCalc = BCCalculatorMetricsTemperature()
        
        if (fromIndex == 0) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // перевести все в Plato
                    self.inputData[2] = BCNumberEntity(title: l("calc.alcohol.formula.plato.begin"), value: gravityCalc.platoFromKgl((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                    self.inputData[4] = BCNumberEntity(title: l("calc.alcohol.formula.plato.end"), value: gravityCalc.platoFromKgl((self.inputData[4] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                case 1:
                    // перевести все в Gravity
                    self.inputData[2] = BCNumberEntity(title: l("calc.alcohol.formula.plato.begin"), value: gravityCalc.kglFromPlato((self.inputData[2] as! BCNumberEntity).numberValue))
                    self.inputData[4] = BCNumberEntity(title: l("calc.alcohol.formula.plato.end"), value: gravityCalc.kglFromPlato((self.inputData[4] as! BCNumberEntity).numberValue))
                default:()
                }
            }
        } else if (fromIndex == 1) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // перевести все в Цельсии
                    self.inputData[3] = BCNumberEntity(title: l("calc.alcohol.formula.temp.begin"), value: tempCalc.celsiiFromFarenheit((self.inputData[3] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                    self.inputData[5] = BCNumberEntity(title: l("calc.alcohol.formula.temp.end"), value: tempCalc.celsiiFromFarenheit((self.inputData[5] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                case 1:
                    // перевести в Фаренгейты
                    self.inputData[3] = BCNumberEntity(title: l("calc.alcohol.formula.temp.begin"), value: tempCalc.farenheitFromCelsii((self.inputData[3] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                    self.inputData[5] = BCNumberEntity(title: l("calc.alcohol.formula.temp.end"), value: tempCalc.farenheitFromCelsii((self.inputData[5] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                default:()
                }
            }
        }
        
        var og = (self.inputData[2] as! BCNumberEntity).numberValue
        var ot = (self.inputData[3] as! BCNumberEntity).numberValue
        var fg = (self.inputData[4] as! BCNumberEntity).numberValue
        var ft = (self.inputData[5] as! BCNumberEntity).numberValue
        
        if let segEntity = self.inputData[0] as? BCSegmentedEntity {
            switch (segEntity.selectedIndex) {
            case 0:()
                // все в Плато
            case 1:
                // перевести в Плато
                og = gravityCalc.platoFromKgl(og)
                fg = gravityCalc.platoFromKgl(fg)
            default:()
            }
        }
        if let segEntity = self.inputData[1] as? BCSegmentedEntity {
            switch (segEntity.selectedIndex) {
            case 0:()
                // все в Цельсиях
            case 1:
                // перевести в Цельсии
                ot = tempCalc.celsiiFromFarenheit(ot)
                ft = tempCalc.celsiiFromFarenheit(ft)
            default:()
            }
        }
        
        (self.outputData[0] as! BCNumberEntity).numberValue = self.abv(og, ot:ot, fg:fg, ft:ft)
    }
    
    func abv(_ og:Double, ot:Double, fg:Double, ft:Double) -> Double {
        let a = -9.944*pow(10.0,-4.0) - 2.031*pow(10.0,-5.0)*ot
        let b = 5.887*pow(10.0,-6.0)*pow(ot,2.0) - 13.578*pow(10.0,-9.0)*pow(ot,3.0)
        let deltaPopn = a + b
        
        let c = -9.944*pow(10.0,-4.0) - 2.031*pow(10.0,-5.0)*ft
        let d = 5.887*pow(10.0,-6.0)*pow(ft,2.0) - 13.578*pow(10.0,-9.0)*pow(ft,3.0)
        let deltaPopk = c + d
        
        let e = 1.00001 + 0.0038661*og
        let f = 1.3488*pow(10.0,-5.0)*pow(og,2.0) + 4.3074*pow(10.0, -8.0)*pow(og, 3.0)
        let edOE = e + f
        let edpopOE = edOE - deltaPopn
        let EdeltaPopn = -668.962 + 1262.45*edpopOE - 776.43*pow(edpopOE,2.0) + 182.94*pow(edpopOE,3.0)
        let g = 1.00001 + 0.0038661*fg
        let h = 1.3488*pow(10.0,-5.0)*pow(fg,2.0) + 4.3074*pow(10.0, -8.0)*pow(fg, 3.0)
        let edAE = g + h
        let edpopAE = edAE - deltaPopk
        let EdeltaPopk = -668.962 + 1262.45*edpopAE - 776.43*pow(edpopAE,2.0) + 182.94*pow(edpopAE,3.0)
        
        let pRE = 0.8114*EdeltaPopk + 0.1886*EdeltaPopn
        
        let pABW = (EdeltaPopn - pRE)/(2.0665 - 0.010665*EdeltaPopn)
        let pABV = pABW/0.7893
        
        return round(100.0 * pABV)/100.0
    }
}
