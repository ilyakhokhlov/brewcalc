//
//  BCMasterViewController.swift
//  BrewCalc
//
//  Created by Ilya on 30.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation
import UIKit

class BCMasterViewController: UITableViewController, UISplitViewControllerDelegate {
    var calculators: Array<BCCalculatorDescription> = [
        BCCalculatorDescription(localizedName: l("calc.metrics"), calculators: [BCCalculatorGravity(), BCCalculatorMetricsVolume(), BCCalculatorMetricsWeight(), BCCalculatorMetricsTemperature()], instructionFilename: ""),
        BCCalculatorDescription(localizedName: l("calc.calorie"), calculators: [BCCalculatorCalorie()], instructionFilename: ""),
        BCCalculatorDescription(localizedName: l("calc.alcohol.table"), calculators: [BCCalculatorAlkoholTable()], instructionFilename: ""),
        BCCalculatorDescription(localizedName: l("calc.alcohol.formula"), calculators: [BCCalculatorAlkoholFormula()], instructionFilename: ""),
        BCCalculatorDescription(localizedName: l("calc.brix"), calculators: [BCCalculatorBrixesGravity()], instructionFilename: isRussian ? "instr_brix_ru" : "instr_brix_en"),
        BCCalculatorDescription(localizedName: l("calc.bittering"), calculators: [BCCalculatorBittering()], instructionFilename: "")
    ]
    var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        self.splitViewController?.delegate = self
        self.navigationItem.title = l("calcs")
        
        let rightButton = UIBarButtonItem(title: l("menu.right.button"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(BCMasterViewController.showAbout))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let splitVC = self.splitViewController as UISplitViewController? {
            if (!splitVC.isCollapsed) {
                if (self.selectedIndexPath == nil) {
                    self.selectedIndexPath = IndexPath(row: 0, section: 0)
                    self.performSegue(withIdentifier: "ShowDetail", sender: nil)
                }
                self.tableView.selectRow(at: self.selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.calculators.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let calculatorDescription = self.calculators[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 16.0)
        cell.textLabel?.text = calculatorDescription.localizedName
        
        cell.setSelected(indexPath == self.selectedIndexPath, animated: false)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if (segue.identifier == "ShowDetail") {
            if let senderCell = sender as? UITableViewCell {
                self.selectedIndexPath = self.tableView.indexPath(for: senderCell)
            }
            let calculatorDescription = self.calculators[self.selectedIndexPath!.row]
            if let destNavVC = segue.destination as? UINavigationController {
                if let destVC = destNavVC.viewControllers[0] as? BCCalculatorTableController {
                    destVC.title = calculatorDescription.localizedName
                    destVC.instructionFilename = calculatorDescription.instructionFilename
                    destVC.calculators = calculatorDescription.calculators
                    
                    GAI.sharedInstance().defaultTracker.send(GAIDictionaryBuilder.createEvent(withCategory: calculatorDescription.localizedName, action: "Calculator opened", label: nil, value: nil).build() as NSDictionary as! [AnyHashable: Any])
                }
            }
        }
    }
    
    @objc func showAbout() {
        self.performSegue(withIdentifier: "ShowAbout", sender: nil)
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {
        if let primaryVC = primaryViewController as? UINavigationController {
            for controller in primaryVC.viewControllers {
                if let calcNavVC = controller as? UINavigationController {
                    if let _ = calcNavVC.visibleViewController as? BCCalculatorTableController {
                        return controller
                    }
                }
            }
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailView = storyboard.instantiateViewController(withIdentifier: "DetailController") as? UINavigationController
        if let controller = detailView?.visibleViewController as? BCCalculatorTableController {
            let indexPath = self.selectedIndexPath == nil ? IndexPath(row: 0, section: 0) : self.selectedIndexPath
            let calculatorDescription = self.calculators[indexPath!.row]
            controller.title = calculatorDescription.localizedName
            controller.instructionFilename = calculatorDescription.instructionFilename
            controller.calculators = calculatorDescription.calculators
            
            GAI.sharedInstance().defaultTracker.send(GAIDictionaryBuilder.createEvent(withCategory: calculatorDescription.localizedName, action: "Calculator opened", label: nil, value: nil).build() as NSDictionary as![AnyHashable: Any])
        }
        
        return detailView
    }
    
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        if let secondaryNavVC = secondaryViewController as? UINavigationController {
            if let calcVC = secondaryNavVC.topViewController as? BCCalculatorTableController {
                if (calcVC.calculators == nil) {
                    return true
                }
            }
        }
        return false
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if let splitVC = self.splitViewController as UISplitViewController? {
            if (!splitVC.isCollapsed) {
                if (self.selectedIndexPath == nil) {
                    self.selectedIndexPath = IndexPath(row: 0, section: 0)
                    self.performSegue(withIdentifier: "ShowDetail", sender: nil)
                }
                self.tableView.selectRow(at: self.selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
            }
        }
    }
}
