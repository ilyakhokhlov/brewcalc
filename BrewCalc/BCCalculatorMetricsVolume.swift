//
//  BCCalculatorMetricsVolume.swift
//  BrewCalc
//
//  Created by Ilya on 05.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

class BCCalculatorMetricsVolume: BCCalculator {
    override init () {
        super.init()
        self.calculatorName = l("metrics.volume")
        self.inputData = [
            BCNumberEntity(title: l("calc.metrics.litres"), value: 0.0),
            BCNumberEntity(title: l("calc.metrics.decalitres"), value: 0.0, numberOfDigits:4),
            BCNumberEntity(title: l("calc.metrics.hectolitres"), value: 0.0, numberOfDigits:5),
            BCNumberEntity(title: l("calc.metrics.millilitres"), value: 0.0, numberOfDigits:0),
            BCNumberEntity(title: l("calc.metrics.enggallon"), value: 0.0, numberOfDigits:4),
            BCNumberEntity(title: l("calc.metrics.engunc"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.metrics.engpint"), value: 0.0),
            BCNumberEntity(title: l("calc.metrics.usgallon"), value: 0.0, numberOfDigits:4),
            BCNumberEntity(title: l("calc.metrics.usunc"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.metrics.uspint"), value: 0.0)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }

        self.outputData = self.inputData

        var source = 0.0
        if (fromIndex < self.inputData.count) {
            source = (self.inputData[fromIndex] as! BCNumberEntity).numberValue
        }
        
        var litres = 0.0
        switch(fromIndex) {
        case 0:
            litres = source
        case 1:
            litres = self.litresFromDecalitres(source)
        case 2:
            litres = self.litresFromHectolitres(source)
        case 3:
            litres = self.litresFromMillilitres(source)
        case 4:
            litres = self.litresFromEngGallon(source)
        case 5:
            litres = self.litresFromEngUnc(source)
        case 6:
            litres = self.litresFromEngPint(source)
        case 7:
            litres = self.litresFromUSGallon(source)
        case 8:
            litres = self.litresFromUSUnc(source)
        case 9:
            litres = self.litresFromUSPint(source)
        default:()
        }

        (self.outputData[0] as! BCNumberEntity).numberValue = litres
        (self.outputData[1] as! BCNumberEntity).numberValue = self.decalitresFromLitres(litres)
        (self.outputData[2] as! BCNumberEntity).numberValue = self.hectolitresFromLitres(litres)
        (self.outputData[3] as! BCNumberEntity).numberValue = self.millilitresFromLitres(litres)
        (self.outputData[4] as! BCNumberEntity).numberValue = self.engGallonFromLitres(litres)
        (self.outputData[5] as! BCNumberEntity).numberValue = self.engUncFromLitres(litres)
        (self.outputData[6] as! BCNumberEntity).numberValue = self.engPintFromLitres(litres)
        (self.outputData[7] as! BCNumberEntity).numberValue = self.usGallonFromLitres(litres)
        (self.outputData[8] as! BCNumberEntity).numberValue = self.usUncFromLitres(litres)
        (self.outputData[9] as! BCNumberEntity).numberValue = self.usPintFromLitres(litres)
    }
    
    func litresFromDecalitres(_ source:Double) -> Double {
        return round(1000.0 * source * 10.0)/1000.0
    }
    
    func litresFromHectolitres(_ source:Double) -> Double {
        return round(1000.0 * source * 100.0)/1000.0
    }
    
    func litresFromMillilitres(_ source:Double) -> Double {
        return round(1000.0 * source / 1000.0)/1000.0
    }
    
    func litresFromEngGallon(_ source:Double) -> Double {
        return round(1000.0 * source * 4.54609188)/1000.0
    }
    
    func litresFromEngUnc(_ source:Double) -> Double {
        return round(1000.0 * source / 1000.0 * 28.413063)/1000.0
    }
    
    func litresFromEngPint(_ source:Double) -> Double {
        return round(1000.0 * source * 0.568261485)/1000.0
    }
    
    func litresFromUSGallon(_ source:Double) -> Double {
        return round(1000.0 * source * 3.78541178)/1000.0
    }
    
    func litresFromUSUnc(_ source:Double) -> Double {
        return round(1000.0 * source / 1000.0 * 29.573531)/1000.0
    }
    
    func litresFromUSPint(_ source:Double) -> Double {
        return round(1000.0 * source * 0.473176473)/1000.0
    }
    
    func decalitresFromLitres(_ source:Double) -> Double {
        return source / 10.0
    }
    
    func hectolitresFromLitres(_ source:Double) -> Double {
        return source / 100.0
    }
    
    func millilitresFromLitres(_ source:Double) -> Double {
        return round(1000.0 * source)
    }
    
    func engGallonFromLitres(_ source:Double) -> Double {
        return round(10000.0 * source / 4.54609188)/10000.0
    }
    
    func engUncFromLitres(_ source:Double) -> Double {
        return round(100.0 * source * 1000.0 / 28.413063)/100.0
    }
    
    func engPintFromLitres(_ source:Double) -> Double {
        return round(1000.0 * source / 0.568261485)/1000.0
    }
    
    func usGallonFromLitres(_ source:Double) -> Double {
        return round(10000.0 * source / 3.78541178)/10000.0
    }
    
    func usUncFromLitres(_ source:Double) -> Double {
        return round(100.0 * source * 1000.0 / 29.573531)/100.0
    }
    
    func usPintFromLitres(_ source:Double) -> Double {
        return round(1000.0 * source / 0.473176473)/1000.0
    }
}
