//
//  BCCalculatorTableController.swift
//  BrewCalc
//
//  Created by Ilya on 01.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

class BCCalculatorTableController: UITableViewController, UITextFieldDelegate {
    var calculators: [BCCalculator]? {
        didSet {
            reloadData()
        }
    }
    var dataSource: [[BCEntity]] = []
    var instructionFilename: String?
    var activeText:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func reloadData() {
        if (self.calculators != nil) {
            self.view.endEditing(true)
            self.dataSource = []
            for calculator in self.calculators! {
                calculator.sectionNumber = self.dataSource.count
                self.dataSource.append(calculator.inputData as AnyObject as! [BCEntity])
                if let _ = calculator as? BCCalculatorSeparated {
                    self.dataSource.append(calculator.outputData as AnyObject as! [BCEntity])
                }
            }
            self.tableView.reloadData()
            if (self.instructionFilename?.isEmpty == false) {
                let infoButton = UIButton(type: UIButtonType.infoLight)
                infoButton.addTarget(self, action: #selector(BCCalculatorTableController.infoButtonAction), for: UIControlEvents.touchUpInside)
                let modalButton = UIBarButtonItem(customView: infoButton)
                self.navigationItem.rightBarButtonItem = modalButton
            } else {
                self.navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    @IBAction func textChanged(_ sender:UITextField) {
        var value = sender.text == nil ? 0.0 : Double(sender.text!)
        if (value == nil) {
            value = 0.0
        }
        let point = self.tableView.convert(sender.center, from: sender.superview)
        let path = self.tableView.indexPathForRow(at: point)
        if (path != nil) {
            let calculator = self.calculatorForSection(path!.section)
            
            if let entity = self.dataSource[path!.section][path!.row] as? BCNumberEntity {
                entity.numberValue = Double(value!)
                entity.used = true
                calculator.calculate(path!.row, newValue: entity)
            } else if let entity = self.dataSource[path!.section][path!.row] as? BCThreeNumbersEntity {
                switch (sender.tag) {
                case 1:
                    entity.number1.numberValue = Double(value!)
                    entity.number1.used = true
                case 2:
                    entity.number2.numberValue = Double(value!)
                    entity.number2.used = true
                case 3:
                    entity.number3.numberValue = Double(value!)
                    entity.number3.used = true
                default:()
                }
                calculator.calculate(path!.row, newValue: entity)
            }
            
            if let separatedCalculator = calculator as? BCCalculatorSeparated {
                self.dataSource[path!.section] = separatedCalculator.inputData as AnyObject as! [BCEntity]
                self.dataSource[path!.section+1] = separatedCalculator.outputData as AnyObject as! [BCEntity]
            } else {
                self.dataSource[path!.section] = calculator.outputData as AnyObject as! [BCEntity]
            }
            
            var indexPathsToReload = [IndexPath]()
            for i in 0..<self.dataSource[path!.section].count {
                if (i != path!.row) {
                    indexPathsToReload.append(IndexPath(row: i, section: path!.section))
                }
            }
            
            if let _ = calculator as? BCCalculatorSeparated {
                for i in 0..<self.dataSource[path!.section+1].count {
                    if (i != path!.row) {
                        indexPathsToReload.append(IndexPath(row: i, section: path!.section+1))
                    }
                }
            }
            
            self.tableView.reloadRows(at: indexPathsToReload, with: UITableViewRowAnimation.none)
        }
    }
    
    @IBAction func segmentedControlChanged(_ sender:UISegmentedControl) {
        let value = sender.selectedSegmentIndex
        let point = self.tableView.convert(sender.center, from: sender.superview)
        let path = self.tableView.indexPathForRow(at: point)
        
        if (path != nil) {
            let calculator = self.calculatorForSection(path!.section)
            let inputEntity = self.dataSource[path!.section][path!.row] as! BCSegmentedEntity
            inputEntity.selectedIndex = Int(value)
            
            calculator.calculate(path!.row, newValue: inputEntity)
            
            if let separatedCalculator = calculator as? BCCalculatorSeparated {
                self.dataSource[path!.section] = separatedCalculator.inputData as AnyObject as! [BCEntity]
                self.dataSource[path!.section+1] = separatedCalculator.outputData as AnyObject as! [BCEntity]
            } else {
                self.dataSource[path!.section] = calculator.outputData as AnyObject as! [BCEntity]
            }
            
            var indexPathsToReload = [IndexPath]()
            for i in 0..<self.dataSource[path!.section].count {
                if (i != path!.row) {
                    indexPathsToReload.append(IndexPath(row: i, section: path!.section))
                }
            }
            
            if let _ = calculator as? BCCalculatorSeparated {
                for i in 0..<self.dataSource[path!.section+1].count {
                    if (i != path!.row) {
                        indexPathsToReload.append(IndexPath(row: i, section: path!.section+1))
                    }
                }
            }
            
            self.tableView.reloadRows(at: indexPathsToReload, with: UITableViewRowAnimation.none)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource[section].count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = self.dataSource[indexPath.section][indexPath.row] as? BCThreeNumbersEntity {
            return 90.0
        }
        return 44.0
    }
    
    let cellId: String = "NumberCellId"
    let segCellId: String = "SegmentedCellId"
    let threeCellId: String = "ThreeNumbersCellId"
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let entity = self.dataSource[indexPath.section][indexPath.row]
        var cell: BCEntityCell?
        
        if let _ = entity as? BCNumberEntity {
            cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? BCEntityCell
        } else if let _ = entity as? BCSegmentedEntity {
            cell = tableView.dequeueReusableCell(withIdentifier: segCellId, for: indexPath) as? BCEntityCell
        } else if let _ = entity as? BCThreeNumbersEntity {
            cell = tableView.dequeueReusableCell(withIdentifier: threeCellId, for: indexPath) as? BCEntityCell
        }
        
        cell?.configureWithEntity(entity)
        
        let calculator = self.calculatorForSection(indexPath.section)
        
        if (calculator is BCCalculatorSeparated && (Int(calculator.sectionNumber) != indexPath.section)) {
            cell?.setEnabled(false)
        } else {
            cell?.setEnabled(true)
        }
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? BCEntityCell
        cell?.becomeResponder()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func calculatorForSection(_ section: Int) -> BCCalculator {
        for calculator in self.calculators! {
            if (Int(calculator.sectionNumber) == section) {
                return calculator
            }
        }
        return self.calculatorForSection(section-1)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let calculator = self.calculatorForSection(section)
        if (Int(calculator.sectionNumber) == section) {
            return calculator.calculatorName
        }
        if let separatedCalculator = calculator as? BCCalculatorSeparated {
            return separatedCalculator.secondName
        }
        return nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let point = self.tableView.convert(textField.center, from: textField.superview)
        let path = self.tableView.indexPathForRow(at: point)
        
        if (path != nil) {
            let entity = self.dataSource[path!.section][path!.row]
            
            var entityToCheck: BCNumberEntity?
            
            if let inputEntity = entity as? BCThreeNumbersEntity {
                switch textField.tag {
                case 1:
                    entityToCheck = inputEntity.number1
                case 2:
                    entityToCheck = inputEntity.number2
                case 3:
                    entityToCheck = inputEntity.number3
                default:()
                }
            } else if let inputEntity = entity as? BCNumberEntity {
                entityToCheck = inputEntity
            }
            
            if (entityToCheck?.used == false) {
                self.activeText = textField.text
                textField.text = ""
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let point = self.tableView.convert(textField.center, from: textField.superview)
        let path = self.tableView.indexPathForRow(at: point)
        
        if (path != nil) {
            let entity = self.dataSource[path!.section][path!.row]
            let numberEntity = entity as? BCNumberEntity
            let threeNumbersEntity = entity as? BCThreeNumbersEntity
            if (numberEntity != nil || threeNumbersEntity != nil) {
                if (textField.text?.count == 0) {
                    textField.text = self.activeText
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func infoButtonAction() {
        self.performSegue(withIdentifier: "ShowInfo", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ShowInfo") {
            if let navVC = segue.destination as? UINavigationController {
                if let vc = navVC.viewControllers[0] as? BCWebViewController {
                    vc.filename = self.instructionFilename
                }
            }
        }
    }
}


