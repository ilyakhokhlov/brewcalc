//
//  BCCalculatorCalorie.swift
//  BrewCalc
//
//  Created by Ilya on 10.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCCalculatorCalorie: BCCalculatorSeparated {
    override init () {
        super.init()
        self.calculatorName = l("calc.calorie.input")
        self.secondName = l("calc.calorie.result")
        self.inputData = [
            BCSegmentedEntity(segments: [l("segment.units.plato"), l("segment.units.gravity")], selectedIndex: 0),
            BCSegmentedEntity(segments: [l("segment.units.litres"), l("segment.units.gallons")], selectedIndex: 0),
            BCNumberEntity(title: l("calc.calorie.volume"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.calorie.og"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.calorie.fg"), value: 0.0, numberOfDigits:2)]
        self.outputData = [
            BCNumberEntity(title: l("calc.calorie.general"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.calorie.100g"), value: 0.0, numberOfDigits:1)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        let gravityCalc = BCCalculatorGravity()
        let volCalc = BCCalculatorMetricsVolume()
        
        if (fromIndex == 0) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // перевести все в Plato
                    self.inputData[3] = BCNumberEntity(title: l("calc.calorie.og"), value: gravityCalc.platoFromKgl((self.inputData[3] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                    self.inputData[4] = BCNumberEntity(title: l("calc.calorie.fg"), value: gravityCalc.platoFromKgl((self.inputData[4] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                case 1:
                    // перевести все в Gravity
                    self.inputData[3] = BCNumberEntity(title: l("calc.calorie.og"), value: gravityCalc.kglFromPlato((self.inputData[3] as! BCNumberEntity).numberValue))
                    self.inputData[4] = BCNumberEntity(title: l("calc.calorie.fg"), value: gravityCalc.kglFromPlato((self.inputData[4] as! BCNumberEntity).numberValue))
                default:()
                }
            }
        } else if (fromIndex == 1) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // перевести все в литры
                    self.inputData[2] = BCNumberEntity(title: l("calc.calorie.volume"), value: volCalc.litresFromUSGallon((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                case 1:
                    // перевести все в галлоны
                    self.inputData[2] = BCNumberEntity(title: l("calc.calorie.volume"), value: volCalc.usGallonFromLitres((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                default:()
                }
            }
        }
        
        var vol = (self.inputData[2] as! BCNumberEntity).numberValue
        var og = (self.inputData[3] as! BCNumberEntity).numberValue
        var fg = (self.inputData[4] as! BCNumberEntity).numberValue
        
        if let segEntity = self.inputData[0] as? BCSegmentedEntity {
            switch (segEntity.selectedIndex) {
            case 0:()
                // все в Плато
            case 1:
                // перевести в Плато
                og = gravityCalc.platoFromKgl(og)
                fg = gravityCalc.platoFromKgl(fg)
            default:()
            }
        }
        if let segEntity = self.inputData[1] as? BCSegmentedEntity {
            switch (segEntity.selectedIndex) {
            case 0:()
                // все в литрах
            case 1:
                // перевести в литры
                vol = volCalc.litresFromUSGallon(vol)
            default:()
            }
        }
        
        let cal = self.caloric(vol, og: og, fg: fg)
        (self.outputData[0] as! BCNumberEntity).numberValue = cal
        (self.outputData[1] as! BCNumberEntity).numberValue = round(10.0*(0.1*cal/vol))/10.0
    }
    
    func caloric(_ volume:Double, og:Double, fg:Double) -> Double {
        /*
        http://www.homebrewer.ru/phpBB3/viewtopic.php?f=9&t=894
        
        4. Расчет истинного экстракта
        RE = 0,8114*AE + 0,1886*OE, где RE - истинный экстракт, AE - видимый экстракт, OE - начальный экстракт (все в градусах Плато);
        
        5. Массовая доля алкоголя в пиве
        ABW = (OE - RE)/(2,0665 - 0,010665*OE), где ABW - массовая доля алкоголя в %, OE - начальный экстракт, RE - истинный экстракт (все в градусах Плато);
        
        6. Объемная доля алкоголя в пиве
        ABV = ABW/0,7893, где ABV - объемная доля алкоголя в %, ABW - массова доля алкоголя в %;
        
        7. Расчет калорийности пива
        EV = 10*V*(3,8*RE + 7,1*ABW + 0,28*RE), где EV - калорийность, V - объем пива в л, RE - истинный экстракт в градусах Плато, ABW - массовая доля алкоголя в %
        */
        
        let re = 0.8114*fg + 0.1886*og
        let abw = (og - re)/(2.0665 - 0.010665*og)
        let ev = 10.0*volume*(3.8*re + 7.1*abw + 0.28*re)
        
        return round(10.0 * ev)/10.0
    }
}
