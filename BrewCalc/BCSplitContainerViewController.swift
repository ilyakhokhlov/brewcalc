//
//  BCSplitContainerViewController.swift
//  BrewCalc
//
//  Created by Ilya on 23.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation
import UIKit

class BCSplitContainerViewController : UISplitViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
        {
            self.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
