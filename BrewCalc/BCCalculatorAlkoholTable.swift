//
//  BCCalculatorAlkoholTable.swift
//  BrewCalc
//
//  Created by Ilya on 11.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCCalculatorAlkoholTable: BCCalculatorSeparated {
    override init () {
        super.init()
        self.calculatorName = l("calc.alcohol.table.input")
        self.secondName = l("calc.alcohol.table.result")
        self.inputData = [
            BCSegmentedEntity(segments: [l("segment.units.plato"), l("segment.units.gravity")], selectedIndex: 0),
            BCNumberEntity(title: l("calc.alcohol.table.plato.begin"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.alcohol.table.plato.end"), value: 0.0, numberOfDigits:2)]
        self.outputData = [
            BCNumberEntity(title: l("calc.alcohol.table.abv"), value: 0.0, numberOfDigits:2)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        let gravityCalc = BCCalculatorGravity()
        
        if (fromIndex == 0) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // перевести все в Plato
                    self.inputData[1] = BCNumberEntity(title: l("calc.alcohol.table.plato.begin"), value: gravityCalc.platoFromKgl((self.inputData[1] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                    self.inputData[2] = BCNumberEntity(title: l("calc.alcohol.table.plato.end"), value: gravityCalc.platoFromKgl((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                case 1:
                    // перевести все в Gravity
                    self.inputData[1] = BCNumberEntity(title: l("calc.alcohol.table.kgl.begin"), value: gravityCalc.kglFromPlato((self.inputData[1] as! BCNumberEntity).numberValue))
                    self.inputData[2] = BCNumberEntity(title: l("calc.alcohol.table.kgl.end"), value: gravityCalc.kglFromPlato((self.inputData[2] as! BCNumberEntity).numberValue))
                default:()
                }
            }
        }

        var og = (self.inputData[1] as! BCNumberEntity).numberValue
        var fg = (self.inputData[2] as! BCNumberEntity).numberValue
        
        if let segEntity = self.inputData[0] as? BCSegmentedEntity {
            switch (segEntity.selectedIndex) {
            case 0:
                // перевести все в Gravity
                og = gravityCalc.kglFromPlato(og)
                fg = gravityCalc.kglFromPlato(fg)
            case 1:()
                // все уже в Gravity
            default:()
            }
        }
        
        (self.outputData[0] as! BCNumberEntity).numberValue = self.abv(og, fg:fg)
    }
    
    func abv(_ og:Double, fg:Double) -> Double {
        return self.abv(og) - self.abv(fg)
    }
    
    func abv(_ kgl:Double) -> Double {
        let sp = [0.0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0,2.25,2.5,2.75,3.0,3.25,3.5,3.75,4.0,4.25,4.5,4.75,5.0,5.25,5.5,5.75,6.0,6.25,6.5,6.75,7.0,7.25,7.5,7.75,8.0,8.25,8.5,8.75,9.0,9.25,9.5,9.75,10.0,10.25,10.5,10.75,11.0,11.25,11.5,11.75,12.0,12.25,12.5,12.75,13.0,13.25,13.5,13.75,14.0,14.25,14.5,14.75]
        
        let gi = [1.002,1.004,1.006,1.008,1.010,1.012,1.014,1.016,1.018,1.020,1.022,1.024,1.026,1.028,1.030,1.032,1.034,1.036,1.038,1.040,1.041,1.043,1.045,1.047,1.049,1.051,1.053,1.055,1.056,1.058,1.060,1.061,1.063,1.065,1.067,1.069,1.071,1.073,1.075,1.076,1.078,1.080,1.082,1.084,1.086,1.088,1.090,1.092,1.093,1.095,1.097,1.098,1.100,1.102,1.104,1.105,1.107,1.109,1.111,1.113]
        
        var res = 0.0
        
        for i in 0..<gi.count-1 {
            let beg = gi[i]
            let end = gi[i+1]
            
            if (kgl >= beg && kgl < end) {
                let cg = kgl - beg
                let pplg = end - beg
                let xg = cg * 100.0 / pplg
                
                let palg = sp[i+1] - sp[i]
                let yg = xg * palg / 100.0
                
                res = sp[i] + yg
                break
            }
        }
        
        return round(100.0 * res)/100.0
    }
}
