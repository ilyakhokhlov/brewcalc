//
//  BCHelpers.swift
//  BrewCalc
//
//  Created by Ilya on 21.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

func l(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

let bcEmail = "brewingcalc@gmail.com"

let isRussian = l("locale") == "ru"
