//
//  BCCalculatorMetricsWeight.swift
//  BrewCalc
//
//  Created by Ilya on 08.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

open class BCCalculatorMetricsWeight: BCCalculator {
    override init () {
        super.init()
        self.calculatorName = l("metrics.weight")
        self.inputData = [
            BCNumberEntity(title: l("calc.metrics.kilogramm"), value: 0.0),
            BCNumberEntity(title: l("calc.metrics.gramm"), value: 0.0, numberOfDigits:0),
            BCNumberEntity(title: l("calc.metrics.unc"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.metrics.funt"), value: 0.0)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        self.outputData = self.inputData
        
        var source = 0.0
        if (fromIndex < self.inputData.count) {
            source = (self.inputData[fromIndex] as! BCNumberEntity).numberValue
        }
        
        var kg = 0.0
        switch(fromIndex) {
        case 0:
            kg = source
        case 1:
            kg = self.kgFromGrames(source)
        case 2:
            kg = self.kgFromUnc(source)
        case 3:
            kg = self.kgFromPounds(source)
        default:()
        }
        
        (self.outputData[0] as! BCNumberEntity).numberValue = kg
        (self.outputData[1] as! BCNumberEntity).numberValue = self.gramesFromKg(kg)
        (self.outputData[2] as! BCNumberEntity).numberValue = self.uncFromKg(kg)
        (self.outputData[3] as! BCNumberEntity).numberValue = self.poundsFromKg(kg)
    }
    
    func kgFromGrames(_ source:Double) -> Double {
        return round(1000.0 * source / 1000.0)/1000.0
    }
    
    func kgFromUnc(_ source:Double) -> Double {
        return round(1000.0 * source * 0.0283495231)/1000.0
    }
    
    func kgFromPounds(_ source:Double) -> Double {
        return round(1000.0 * source * 0.45359237)/1000.0
    }
    
    func gramesFromKg(_ source:Double) -> Double {
        return round(source * 1000.0)
    }
    
    func uncFromKg(_ source:Double) -> Double {
        return round(100.0 * source / 0.0283495231)/100.0
    }
    
    func poundsFromKg(_ source:Double) -> Double {
        return round(1000.0 * source / 0.45359237)/1000.0
    }
    
    open func uncFromGr(_ source:Double) -> Double {
        return self.uncFromKg(self.kgFromGrames(source))
    }
    
    open func grFromUnc(_ source:Double) -> Double {
        return self.gramesFromKg(self.kgFromUnc(source))
    }
}
