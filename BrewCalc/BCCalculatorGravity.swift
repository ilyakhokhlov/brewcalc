//
//  BCCalculatorGravity.swift
//  BrewCalc
//
//  Created by Ilya on 08.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

class BCCalculatorGravity: BCCalculator {
    override init () {
        super.init()
        self.calculatorName = l("calc.gravity")
        self.inputData = [
            BCNumberEntity(title: l("calc.gravity.plato"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.gravity.kgl"), value: 0.0)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        self.outputData = self.inputData
        
        var source = 0.0
        if (fromIndex < self.inputData.count) {
            source = (self.inputData[fromIndex] as! BCNumberEntity).numberValue
        }
        
        var kgl = 0.0
        switch(fromIndex) {
        case 0:
            kgl = self.kglFromPlato(source)
        case 1:
            kgl = source
        default:()
        }
        
        (self.outputData[0] as! BCNumberEntity).numberValue = self.platoFromKgl(kgl)
        (self.outputData[1] as! BCNumberEntity).numberValue = kgl
    }
    
    func kglFromPlato(_ source:Double) -> Double {
        return round(1000.0*(1.0 + (source / (258.6 - ((source/258.2) * 227.1)))))/1000.0
    }
    
    func platoFromKgl(_ source:Double) -> Double {
        return round(100.0*(258.6/(1.0/(source-1.0) + 227.1/258.2)))/100.0
    }
}
