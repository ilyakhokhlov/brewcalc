//
//  BCCalculatorDescription.swift
//  BrewCalc
//
//  Created by Ilya on 30.06.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

struct BCCalculatorDescription {
    var localizedName: String = ""
    var calculators: Array<BCCalculator> = []
    var instructionFilename: String = ""
    
    init(localizedName name: String, calculators calcs: Array<BCCalculator>, instructionFilename filename: String) {
        self.localizedName = name
        self.calculators = calcs
        self.instructionFilename = filename
    }
}