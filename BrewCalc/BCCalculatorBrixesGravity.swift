//
//  BCCalculatorBrixesGravity.swift
//  BrewCalc
//
//  Created by Ilya on 12.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCCalculatorBrixesGravity: BCCalculatorSeparated {
    override init () {
        super.init()
        self.calculatorName = l("calc.brixes.gravity.input")
        self.secondName = l("calc.brixes.gravity.output")
        self.inputData = [
            BCSegmentedEntity(segments: [l("segment.units.plato"), l("segment.units.gravity")], selectedIndex: 0),
            BCSegmentedEntity(segments: [l("segment.refractometer.brix"), l("segment.refractometer.gravity")], selectedIndex: 0),
            BCNumberEntity(title: l("calc.brixes.gravity.kpd"), value: 1.04, numberOfDigits:2),
            BCNumberEntity(title: l("calc.brixes.gravity.ob"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.brixes.gravity.fb"), value: 0.0, numberOfDigits:2)]
        self.outputData = [
            BCNumberEntity(title: l("calc.brixes.gravity.og"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: 0.0, numberOfDigits:2),
            BCNumberEntity(title: l("calc.brixes.gravity.abv"), value: 0.0, numberOfDigits:2)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        let gravityCalc = BCCalculatorGravity()
        
        let koef = (self.inputData[2] as! BCNumberEntity).numberValue
        
        if (fromIndex == 0) {
            if let segEntity = newValue as? BCSegmentedEntity {
                let segEntity2 = self.inputData[1] as! BCSegmentedEntity
                switch (segEntity2.selectedIndex) {
                case 0:// 2-нп в brix, 3-кп в brix, 0-нп в кг/л, 1-кп в кг/л
                    switch (segEntity.selectedIndex)
                    {
                    case 0://переводим в плато
                        self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.platoFromKgl((self.outputData[0] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                        self.outputData[1] = BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: gravityCalc.platoFromKgl((self.outputData[1] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                    case 1://переводим в кг/л
                        self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.kglFromPlato((self.outputData[0] as! BCNumberEntity).numberValue))
                        self.outputData[1] = BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: gravityCalc.kglFromPlato((self.outputData[1] as! BCNumberEntity).numberValue))
                    default:()
                    }
                case 1:// 2-кп в кг/л, 3-кп в brix, 0-нп в кг/л, 1-нп в brix
                    switch (segEntity.selectedIndex)
                    {
                    case 0://переводим в плато
                        self.inputData[3] = BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: gravityCalc.platoFromKgl((self.inputData[3] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                        self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.platoFromKgl((self.outputData[0] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                    case 1://переводим в кг/л
                        self.inputData[3] = BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: gravityCalc.kglFromPlato((self.inputData[3] as! BCNumberEntity).numberValue))
                        self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.kglFromPlato((self.outputData[0] as! BCNumberEntity).numberValue))
                    default:()
                    }
                default:()
                }
            }
        } else if (fromIndex == 1) {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex) {
                case 0:
                    // 2-кп в плато, 3-кп в brix, 0-нп в плато, 1-нп в brix в
                    // 2-нп в brix, 3-кп в brix, 0-нп в плато, 1-кп в плато
                    
                    let ogbrix = self.outputData[1] as! BCNumberEntity
                    self.outputData[1] = self.inputData[3]
                    self.inputData[3] = BCNumberEntity(title: ogbrix.numberTitle, value: round(100.0*ogbrix.numberValue*koef)/100.0, numberOfDigits: ogbrix.numberOfDigits)
                    
                case 1:
                    // 2-нп в brix, 3-кп в brix, 0-нп в плато, 1-кп в плато в
                    // 2-кп в плато, 3-кп в brix, 0-нп в плато, 1-нп в brix
                    
                    let ogbrix = self.inputData[3] as! BCNumberEntity
                    self.inputData[3] = self.outputData[1]
                    self.outputData[1] = ogbrix
                    
                default:()
                }
            }
        }
        
        var inp1 = (self.inputData[3] as! BCNumberEntity).numberValue
        let inp2 = (self.inputData[4] as! BCNumberEntity).numberValue
        var out1 = 0.0
        var out2 = 0.0
        var out3 = 0.0
        
        let segEntity = self.inputData[0] as! BCSegmentedEntity
        let segEntity2 = self.inputData[1] as! BCSegmentedEntity
        switch (segEntity2.selectedIndex)
        {
        case 0:// 2-нп в brix, 3-кп в brix, 0-нп в кг/л, 1-кп в кг/л
            out1 = self.gravity(inp1/koef)
            out2 = self.gravity(inp1/koef, fb:inp2/koef)
            out3 = self.abv(inp2/koef, cg:out2)
            (self.outputData[0] as! BCNumberEntity).numberValue = out1
            (self.outputData[1] as! BCNumberEntity).numberValue = out2
            (self.outputData[2] as! BCNumberEntity).numberValue = out3
            switch (segEntity.selectedIndex)
            {
            case 0://переводим в плато
                self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.platoFromKgl((self.outputData[0] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                self.outputData[1] = BCNumberEntity(title: l("calc.brixes.gravity.fg"), value: gravityCalc.platoFromKgl((self.outputData[1] as! BCNumberEntity).numberValue), numberOfDigits: 2)
            default:()
            }
        case 1:// 2-кп в кг/л, 3-кп в brix, 0-нп в кг/л, 1-нп в brix
            switch (segEntity.selectedIndex)
            {
            case 0://переводим в кг/л
                inp1 = gravityCalc.kglFromPlato(inp1)
            default:()
            }
            out3 = self.abv(inp2/koef, cg:inp1)
            out1 = self.og(inp2/koef, cg:inp1)
            out2 = self.brix(out1)
            (self.outputData[0] as! BCNumberEntity).numberValue = out1
            (self.outputData[1] as! BCNumberEntity).numberValue = out2
            (self.outputData[2] as! BCNumberEntity).numberValue = out3
            switch (segEntity.selectedIndex)
            {
            case 0://переводим в плато
                self.outputData[0] = BCNumberEntity(title: l("calc.brixes.gravity.og"), value: gravityCalc.platoFromKgl(out1), numberOfDigits: 2)
            default:()
            }
        default:()
        }
    }
    
    func gravity(_ fromOB:Double, fb:Double) -> Double {
        let g =
            1.001843 -
            0.002318474*fromOB -
            0.000007775*pow(fromOB, 2.0) -
            0.000000034*pow(fromOB, 3.0) +
            0.00574*fb +
            0.00003344*pow(fb, 2.0) +
            0.000000086*pow(fb, 3.0)
        return round(1000.0 * g)/1000.0
    }
    
    func gravity(_ fromBrix:Double) -> Double {
        /*
        Brix -> SG Equation:
        SG = (Brix / (258.6-((Brix / 258.2)*227.1))) + 1
        (Source: Brew Your Own Magazine)
        */
        return round(1000.0 * (fromBrix / (258.6 - ((fromBrix / 258.2) * 227.1)) + 1.0))/1000.0
    }
    
    func brix(_ fromGravity:Double) -> Double {
        /*
        SG -> Brix Equation:
        Brix = (((182.4601 * SG -775.6821) * SG +1262.7794) * SG -669.5622)
        (Source: http://en.wikipedia.org/wiki/Brix)
        */
        return round(100.0 * (((182.4601 * fromGravity - 775.6821) * fromGravity + 1262.7794) * fromGravity - 669.5622))/100.0
    }
    
    func abv(_ fromCB:Double, cg:Double) -> Double {
        let abv = (277.8851 - 277.4*cg + 0.9956*fromCB + 0.00523*pow(fromCB, 2.0) + 0.000013*pow(fromCB, 3.0)) * (cg/0.79)
        return round(100.0 * abv)/100.0
    }
    
    func og(_ fromCB:Double, cg:Double) -> Double {
        let j = (100.0*((194.5935 + (129.8*cg) + ((1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))*((410.8815*(1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))) - 790.8732))) + (2.0665*(1017.5596 - (277.4*cg) + ((1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))*((937.8135*(1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))) - 1805.1228)))))) / (100.0 + (1.0665*(1017.5596 - (277.4*cg) + ((1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))*((937.8135*(1.33302 + (0.001427193*fromCB) + (0.000005791157*pow(fromCB, 2.0)))) - 1805.1228)))))
        
        let og = (j / (258.6 - ((j / 258.2) * 227.1))) + 1.0
        
        return round(1000.0 * og)/1000.0
    }
}

