//
//  BCNumberEntity.swift
//  BrewCalc
//
//  Created by Ilya on 03.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCNumberEntity: BCEntity {
    var numberValue: Double = 0.0
    var numberTitle: String = ""
    var numberOfDigits: Int = 3
    var used: Bool = false
    
    init(title: String, value: Double) {
        self.numberValue = value
        self.numberTitle = title
        super.init()
    }
    
    init(title: String, value: Double, numberOfDigits: Int) {
        self.numberOfDigits = numberOfDigits
        self.numberValue = value
        self.numberTitle = title
        super.init()
    }
    
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! BCNumberEntity
        copy.numberValue = self.numberValue
        copy.numberTitle = self.numberTitle
        copy.numberOfDigits = self.numberOfDigits
        copy.used = self.used
        return copy
    }
}
