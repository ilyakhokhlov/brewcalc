//
//  BCSegmentedEntity.swift
//  BrewCalc
//
//  Created by Ilya on 04.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCSegmentedEntity: BCEntity {
    var selectedIndex: Int = 0
    var segments: [String] = []
    
    init(segments: [String], selectedIndex: Int) {
        self.segments = segments
        self.selectedIndex = selectedIndex
        super.init()
    }
    
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! BCSegmentedEntity
        copy.selectedIndex = self.selectedIndex
        copy.segments = self.segments
        return copy
    }
}
