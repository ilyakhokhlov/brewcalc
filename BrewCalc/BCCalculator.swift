//
//  BCCalculator.swift
//  BrewCalc
//
//  Created by Ilya on 05.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import Foundation

open class BCCalculator: NSObject {
    var inputData: [BCEntity] = []
    var outputData: [BCEntity] = []
    var calculatorName: String = ""
    var sectionNumber: Int = 0
    
    func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        for entity in self.inputData {
            if let numberEntity = entity as? BCNumberEntity {
                numberEntity.used = true
            }
        }
    }
}
