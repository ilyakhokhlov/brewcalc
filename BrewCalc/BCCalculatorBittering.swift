//
//  BCCalculatorBittering.swift
//  BrewCalc
//
//  Created by Ilya on 12.07.15.
//  Copyright (c) 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCCalculatorBittering: BCCalculatorSeparated {
    override init () {
        super.init()
        self.calculatorName = l("calc.bittering.input")
        self.secondName = l("calc.bittering.output")
        self.inputData = [
            BCSegmentedEntity(segments: [l("segment.units.metric"), l("segment.units.us")], selectedIndex: 0),
            BCSegmentedEntity(segments: [l("segment.units.plato"), l("segment.units.gravity")], selectedIndex: 0),
            BCNumberEntity(title: l("calc.bittering.volume.litres"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.gravity.plato"), value: 0.0, numberOfDigits:2),
            BCThreeNumbersEntity(title: l("calc.bittering.hop1.params"), number1: BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: 0.0, numberOfDigits: 1), number2: BCNumberEntity(title: l("calc.bittering.hop.param.alpha"), value: 0.0, numberOfDigits: 1), number3: BCNumberEntity(title: l("calc.bittering.hop.param.min"), value: 0.0, numberOfDigits: 1)),
            BCThreeNumbersEntity(title: l("calc.bittering.hop2.params"), number1: BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: 0.0, numberOfDigits: 1), number2: BCNumberEntity(title: l("calc.bittering.hop.param.alpha"), value: 0.0, numberOfDigits: 1), number3: BCNumberEntity(title: l("calc.bittering.hop.param.min"), value: 0.0, numberOfDigits: 1)),
            BCThreeNumbersEntity(title: l("calc.bittering.hop3.params"), number1: BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: 0.0, numberOfDigits: 1), number2: BCNumberEntity(title: l("calc.bittering.hop.param.alpha"), value: 0.0, numberOfDigits: 1), number3: BCNumberEntity(title: l("calc.bittering.hop.param.min"), value: 0.0, numberOfDigits: 1)),
            BCThreeNumbersEntity(title: l("calc.bittering.hop4.params"), number1: BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: 0.0, numberOfDigits: 1), number2: BCNumberEntity(title: l("calc.bittering.hop.param.alpha"), value: 0.0, numberOfDigits: 1), number3: BCNumberEntity(title: l("calc.bittering.hop.param.min"), value: 0.0, numberOfDigits: 1)),
            BCThreeNumbersEntity(title: l("calc.bittering.hop5.params"), number1: BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: 0.0, numberOfDigits: 1), number2: BCNumberEntity(title: l("calc.bittering.hop.param.alpha"), value: 0.0, numberOfDigits: 1), number3: BCNumberEntity(title: l("calc.bittering.hop.param.min"), value: 0.0, numberOfDigits: 1))]
        self.outputData = [
            BCNumberEntity(title: l("calc.bittering.result"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.result.hop1"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.result.hop2"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.result.hop3"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.result.hop4"), value: 0.0, numberOfDigits:1),
            BCNumberEntity(title: l("calc.bittering.result.hop5"), value: 0.0, numberOfDigits:1)]
    }
    
    override func calculate(_ fromIndex: Int, newValue: BCEntity?) {
        super.calculate(fromIndex, newValue: newValue)
        
        if let valueToSet = newValue {
            self.inputData[fromIndex] = valueToSet
        }
        
        let gravityCalc = BCCalculatorGravity()
        let volCalc = BCCalculatorMetricsVolume()
        let weightCalc = BCCalculatorMetricsWeight()
        
        if (fromIndex == 0)
        {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex)
                {
                case 0://переводим в метрическую систему
                    self.inputData[2] = BCNumberEntity(title: l("calc.bittering.volume.litres"), value: volCalc.litresFromUSGallon((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                    (self.inputData[4] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: weightCalc.grFromUnc((self.inputData[4] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[5] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: weightCalc.grFromUnc((self.inputData[5] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[6] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: weightCalc.grFromUnc((self.inputData[6] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[7] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: weightCalc.grFromUnc((self.inputData[7] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[8] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.gram"), value: weightCalc.grFromUnc((self.inputData[8] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                case 1://переводим в амер. систему
                    self.inputData[2] = BCNumberEntity(title: l("calc.bittering.volume.gal"), value: volCalc.usGallonFromLitres((self.inputData[2] as! BCNumberEntity).numberValue), numberOfDigits: 1)
                    (self.inputData[4] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.oz"), value: weightCalc.uncFromGr((self.inputData[4] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[5] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.oz"), value: weightCalc.uncFromGr((self.inputData[5] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[6] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.oz"), value: weightCalc.uncFromGr((self.inputData[6] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[7] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.oz"), value: weightCalc.uncFromGr((self.inputData[7] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                    (self.inputData[8] as! BCThreeNumbersEntity).number1 = BCNumberEntity(title: l("calc.bittering.hop.param.weight.oz"), value: weightCalc.uncFromGr((self.inputData[8] as! BCThreeNumbersEntity).number1.numberValue), numberOfDigits: 1)
                default:()
                }
            }
        }
        else if (fromIndex == 1)
        {
            if let segEntity = newValue as? BCSegmentedEntity {
                switch (segEntity.selectedIndex)
                {
                case 0://переводим в плато
                    self.inputData[3] = BCNumberEntity(title: l("calc.bittering.gravity.plato"), value: gravityCalc.platoFromKgl((self.inputData[3] as! BCNumberEntity).numberValue), numberOfDigits: 2)
                case 1://переводим в кг/л
                    self.inputData[3] = BCNumberEntity(title: l("calc.bittering.gravity.sg"), value: gravityCalc.kglFromPlato((self.inputData[3] as! BCNumberEntity).numberValue))
                default:()
                }
            }
        }
        
        var vol = (self.inputData[2] as! BCNumberEntity).numberValue
        var grav = (self.inputData[3] as! BCNumberEntity).numberValue
        var weight1 = (self.inputData[4] as! BCThreeNumbersEntity).number1.numberValue
        let alpha1 = (self.inputData[4] as! BCThreeNumbersEntity).number2.numberValue
        let min1 = (self.inputData[4] as! BCThreeNumbersEntity).number3.numberValue
        var weight2 = (self.inputData[5] as! BCThreeNumbersEntity).number1.numberValue
        let alpha2 = (self.inputData[5] as! BCThreeNumbersEntity).number2.numberValue
        let min2 = (self.inputData[5] as! BCThreeNumbersEntity).number3.numberValue
        var weight3 = (self.inputData[6] as! BCThreeNumbersEntity).number1.numberValue
        let alpha3 = (self.inputData[6] as! BCThreeNumbersEntity).number2.numberValue
        let min3 = (self.inputData[6] as! BCThreeNumbersEntity).number3.numberValue
        var weight4 = (self.inputData[7] as! BCThreeNumbersEntity).number1.numberValue
        let alpha4 = (self.inputData[7] as! BCThreeNumbersEntity).number2.numberValue
        let min4 = (self.inputData[7] as! BCThreeNumbersEntity).number3.numberValue
        var weight5 = (self.inputData[8] as! BCThreeNumbersEntity).number1.numberValue
        let alpha5 = (self.inputData[8] as! BCThreeNumbersEntity).number2.numberValue
        let min5 = (self.inputData[8] as! BCThreeNumbersEntity).number3.numberValue
        
        let segEntity = self.inputData[0] as! BCSegmentedEntity
        if (segEntity.selectedIndex == 0)
        {
            vol = volCalc.usGallonFromLitres(vol)
            weight1 = weightCalc.uncFromGr(weight1)
            weight2 = weightCalc.uncFromGr(weight2)
            weight3 = weightCalc.uncFromGr(weight3)
            weight4 = weightCalc.uncFromGr(weight4)
            weight5 = weightCalc.uncFromGr(weight5)
        }
        let segEntity2 = self.inputData[1] as! BCSegmentedEntity
        if (segEntity2.selectedIndex == 0)
        {
            grav = gravityCalc.kglFromPlato(grav)
        }
        
        let ibu1 = self.ibu(alpha1, weight: weight1, time: min1, volume: vol, gravity: grav)
        let ibu2 = self.ibu(alpha2, weight: weight2, time: min2, volume: vol, gravity: grav)
        let ibu3 = self.ibu(alpha3, weight: weight3, time: min3, volume: vol, gravity: grav)
        let ibu4 = self.ibu(alpha4, weight: weight4, time: min4, volume: vol, gravity: grav)
        let ibu5 = self.ibu(alpha5, weight: weight5, time: min5, volume: vol, gravity: grav)
        let ibu = ibu1+ibu2+ibu3+ibu4+ibu5
        
        (self.outputData[0] as! BCNumberEntity).numberValue = ibu
        (self.outputData[1] as! BCNumberEntity).numberValue = ibu1
        (self.outputData[2] as! BCNumberEntity).numberValue = ibu2
        (self.outputData[3] as! BCNumberEntity).numberValue = ibu3
        (self.outputData[4] as! BCNumberEntity).numberValue = ibu4
        (self.outputData[5] as! BCNumberEntity).numberValue = ibu5
    }
    
    func ibu(_ fromAlpha:Double, weight:Double, time:Double, volume:Double, gravity:Double) -> Double {
        let someAlpha = fromAlpha/100.0
        let someWeight = weight*7490.0
        let someVolume = volume*1.65
        let someGravity = pow(0.000125, gravity-1.0)
        let someTime = 1.0-exp(-0.04*time)
        return someAlpha*someWeight/someVolume*someGravity*someTime/4.15
    }


}
