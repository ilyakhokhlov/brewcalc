//
//  BCThreeNumbersEntity.swift
//  BrewCalc
//
//  Created by Ilya on 04.07.15.
//  Copyright © 2015 Ilya Khokhlov. All rights reserved.
//

import UIKit

class BCThreeNumbersEntity: BCEntity {
    var number1: BCNumberEntity
    var number2: BCNumberEntity
    var number3: BCNumberEntity
    var numberTitle: String
    
    init(title: String, number1: BCNumberEntity, number2: BCNumberEntity, number3: BCNumberEntity) {
        self.numberTitle = title
        self.number1 = number1
        self.number2 = number2
        self.number3 = number3
        super.init()
    }
    
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! BCThreeNumbersEntity
        copy.number1 = self.number1.copy(with: zone) as! BCNumberEntity
        copy.number2 = self.number2.copy(with: zone) as! BCNumberEntity
        copy.number3 = self.number3.copy(with: zone) as! BCNumberEntity
        copy.numberTitle = self.numberTitle
        return copy
    }
}
